package org.my.example.app.internal;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.my.example.app.rs.internal.models.AuthorDTO;
import org.my.example.app.test.AbstractTest;
import org.my.example.app.utils.JWTUtils;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import java.time.OffsetDateTime;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
@WithDBData(value = {"tkit-quarkus-bookstore-test.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class AuthorRestControllerTest extends AbstractTest {

    private static final int AMOUNT_OF_AUTHORS = 4;
    private static String default_valid_token;

    @BeforeAll
    public static void setUp() throws Exception {
        default_valid_token = JWTUtils.generateTokenString("/data/test_token_valid.json", null);
    }

    @Test
    @DisplayName("create author")
    void testSuccessCreateAuthor() {
        AuthorDTO author = new AuthorDTO();
        author.setFirstName("William");
        author.setSurname("Shakespeare");
        author.setAge(50);
        author.setBirthDate(OffsetDateTime.now());
        author.setAddress("williamstrasse 8");

        AuthorDTO authorDTO = given()
                .header("Authorization", "Bearer " + default_valid_token)
                .contentType(APPLICATION_JSON)
                .body(author)
                .post("/authors/")
                .prettyPeek()
                .then()
                .statusCode(201)
                .extract().body().as(AuthorDTO.class);
        assertEquals(author.getSurname(), authorDTO.getSurname());
        assertEquals(author.getAddress(), authorDTO.getAddress());

        PageResultDTO pageResultDTO = given()
                .header("Authorization", "Bearer " + default_valid_token)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .when()
                .get("/authors/")
                .then()
                .statusCode(200)
                .extract().body().as(PageResultDTO.class);
        assertThat(pageResultDTO.getTotalElements()).isEqualTo(AMOUNT_OF_AUTHORS + 1);
    }

    private TypeRef<PageResultDTO<AuthorDTO>> getAuthorDtoTypeRef() {
        return new TypeRef<>() {
        };
    }
}