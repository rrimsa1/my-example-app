package org.my.example.app.internal;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.my.example.app.domain.daos.AuthorDAO;
import org.my.example.app.domain.models.entities.Author;
import org.my.example.app.domain.models.entities.Book;
import org.my.example.app.rs.internal.mappers.AuthorMapper;
import org.my.example.app.rs.internal.mappers.BookMapper;
import org.my.example.app.rs.internal.models.AuthorDTO;
import org.my.example.app.rs.internal.models.BookCreateDTO;
import org.my.example.app.rs.internal.models.BookDTO;
import org.my.example.app.test.AbstractTest;
import org.my.example.app.utils.JWTUtils;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
@WithDBData(value = {"tkit-quarkus-books-test.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class BookRestControllerTest extends AbstractTest {
    private static final int AMOUNT_OF_AUTHORS = 4;
    private static final int AMOUNT_OF_BOOKS = 4;
    private static String default_valid_token;
    @Inject
    AuthorMapper authorMapper;

    @Inject
    BookMapper bookMapper;

    @Inject
    AuthorDAO authorDAO;
    @BeforeAll
    public static void setUp() throws Exception {
        default_valid_token = JWTUtils.generateTokenString("/data/test_token_valid.json", null);
    }

    @Test
    @DisplayName("create book")
    void testSuccessCreateBook() {

        //first create an Author so we have a valid ID
//        AuthorDTO author = new AuthorDTO();
//        author.setFirstName("William");
//        author.setSurname("Shakespeare");
//        author.setAge(50);
//        author.setBirthDate(OffsetDateTime.now());
//        author.setAddress("williamstrasse 8");
//        author.setId("1");

//        public AuthorDTO getAuthorById(String id) {
//            author = authorDAO.findById(author.getId());
//            if (Objects.isNull(author)) {
//                throw new RestException(Response.Status.NOT_FOUND,
//                        Response.Status.NOT_FOUND,
//                        String.format(AUTHOR_NOT_FOUND_ERROR, id));
//            }
//            return authorMapper.map(author);
//        }
        Author author = new Author();
        author.setId("1");
        author.setFirstName("William");
        author.setSurname("Shakespeare");
        author.setAge(50);
        author.setBirthDate(LocalDateTime.now());
        author.setAddress("williamstrasse 8");

        AuthorDTO authorDTO = given()
                .header("Authorization", "Bearer " + default_valid_token)
                .contentType(APPLICATION_JSON)
                .body(author)
                .post("/authors/")
                .prettyPeek()
                .then()
                .statusCode(201)
                .extract().body().as(AuthorDTO.class);
        assertEquals(author.getSurname(), authorDTO.getSurname());
        assertEquals(author.getAddress(), authorDTO.getAddress());

        Book book = new Book();
        book.setTitle("HarryPotter");
        book.setCategory("Sci-Fi");
        book.setIsbn(453);
        book.setDateOfPublication(LocalDateTime.now());
        book.setNumberOfPages(455);
        book.setAuthor(authorDAO.findById("1"));




        BookDTO bookDTO = given()
                .header("Authorization", "Bearer " + default_valid_token)
                .contentType(APPLICATION_JSON)
                .body(book)
                .post("/books")
                .prettyPeek()
                .then()
                .statusCode(201)
                .extract().body().as(BookDTO.class);
        assertEquals(book.getTitle(), bookDTO.getTitle());
        assertEquals(book.getIsbn(), bookDTO.getIsbn());

        PageResultDTO pageResultDTO = given()
                .header("Authorization", "Bearer " + default_valid_token)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .when()
                .get("/books/")
                .then()
                .statusCode(200)
                .extract().body().as(PageResultDTO.class);
        assertThat(pageResultDTO.getTotalElements()).isEqualTo(AMOUNT_OF_BOOKS + 1);
    }

    private TypeRef<PageResultDTO<AuthorDTO>> getAuthorDtoTypeRef() {
        return new TypeRef<>() {
        };
    }
}