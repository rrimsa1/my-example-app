package org.my.example.app.domain.daos;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.my.example.app.domain.models.criteria.AuthorSearchCriteria;
import org.my.example.app.test.AbstractTest;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class AuthorDAOTest extends AbstractTest {

    @Test
    public void shouldThrowDAOExceptionWhenTryFindUserByCriteriaEqualNull() {
        //given
        AuthorDAO authorDAO = new AuthorDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        AuthorSearchCriteria searchCriteria = null;
        //when
        DAOException throwException = Assertions.assertThrows(DAOException.class, () -> {
            authorDAO.searchByCriteria(searchCriteria);
        });
        assertThat(throwException.key).isEqualTo(AuthorDAO.ErrorKeys.ERROR_FIND_AUTHORS_SEARCH_CRITERIA_REQUIRED);
    }

    @Test
    public void shouldThrowDAOExceptionWhenTryFindAuthorByEmptyCriteria() {
        //given
        AuthorDAO authorDAO = new AuthorDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        AuthorSearchCriteria searchCriteria = new AuthorSearchCriteria();
        //when
        DAOException thrownException = Assertions.assertThrows(DAOException.class, () -> {
            authorDAO.searchByCriteria(searchCriteria);
        });
        //then
        assertThat(thrownException.key).isEqualTo(AuthorDAO.ErrorKeys.ERROR_FIND_AUTHORS_BY_CRITERIA);
    }

}