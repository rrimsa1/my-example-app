package org.my.example.app.domain.daos;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.my.example.app.domain.models.criteria.AuthorSearchCriteria;
import org.my.example.app.domain.models.criteria.BookSearchCriteria;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class BookDAOTest {

    @Test
    public void shouldThrowDAOExceptionWhenTryFindUserByCriteriaEqualNull() {
        //given
        BookDAO bookDAO = new BookDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        BookSearchCriteria searchCriteria = null;
        //when
        DAOException throwException = Assertions.assertThrows(DAOException.class, () -> {
            bookDAO.searchByCriteria(searchCriteria);
        });
        assertThat(throwException.key).isEqualTo(BookDAO.ErrorKeys.ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED);
    }

    @Test
    public void shouldThrowDAOExceptionWhenTryFindBookByEmptyCriteria() {
        //given
        BookDAO bookDAO = new BookDAO() {
            @Override
            protected EntityManager getEntityManager() {
                return null;
            }
        };
        BookSearchCriteria searchCriteria = new BookSearchCriteria();
        //when
        DAOException thrownException = Assertions.assertThrows(DAOException.class, () -> {
            bookDAO.searchByCriteria(searchCriteria);
        });
        //then
        assertThat(thrownException.key).isEqualTo(BookDAO.ErrorKeys.ERROR_FIND_BOOKS_BY_CRITERIA);
    }

}