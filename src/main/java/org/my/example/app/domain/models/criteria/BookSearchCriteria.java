package org.my.example.app.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;
import org.my.example.app.domain.models.entities.Author;

import java.time.LocalDateTime;

@Getter
@Setter
public class BookSearchCriteria {

    private String id;

    private String title;

    private String category;

    private Integer isbn;

    private Integer numberOfPages;

    private LocalDateTime dateOfPublication;

    private String authorId;

    /**
     * The number of page for Hibernate pagination.
     */
    private Integer pageNumber;
    /**
     * The size of page for Hibernate pagination.
     */
    private Integer pageSize;

}



