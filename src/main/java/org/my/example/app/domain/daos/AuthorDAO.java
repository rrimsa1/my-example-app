package org.my.example.app.domain.daos;

import org.my.example.app.domain.models.criteria.AuthorSearchCriteria;
import org.my.example.app.domain.models.entities.Author;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class AuthorDAO extends AbstractDAO<Author> {

    public PageResult<Author> searchByCriteria(AuthorSearchCriteria criteria) {

        if (criteria == null) {
            throw new DAOException(
                    ErrorKeys.ERROR_FIND_AUTHORS_SEARCH_CRITERIA_REQUIRED,
                    new NullPointerException()
            );
        }
        try {

            CriteriaQuery<Author> cq = criteriaQuery();
            Root<Author> root = cq.from(Author.class);


            List<Predicate> predicates = new ArrayList<>();

            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();


            if (criteria.getFirstName() != null && !criteria.getFirstName().isEmpty()) {
                predicates.add(cb.like(
                        cb.lower(root.get("firstName")),
                        criteria.getFirstName().toLowerCase() + "%"));
            }


            if (criteria.getSurname() != null && !criteria.getSurname().isEmpty()) {
                predicates.add(cb.like(
                        cb.lower(root.get("surname")),
                        criteria.getSurname().toLowerCase() + "%"));
            }

            if (criteria.getAge() != null) {
                predicates.add(cb.equal(root.get("age"), criteria.getAge()));
            }

            if (criteria.getBirthDate() != null) {
                predicates.add(
                        cb.lessThanOrEqualTo(
                                root.get("birthDate"),
                                criteria.getBirthDate()
                        )
                );
            }


            if (!predicates.isEmpty()) {
                cq.where(predicates.toArray(new Predicate[0]));
            }

            return createPageQuery(cq, Page.of(criteria.getPageNumber(),
                    criteria.getPageSize())).getPageResult();

        } catch (Exception e) {
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHORS_BY_CRITERIA, e);
        }
    }

    enum ErrorKeys {
        ERROR_FIND_AUTHORS_BY_CRITERIA,
        ERROR_FIND_AUTHORS_SEARCH_CRITERIA_REQUIRED
    }
}