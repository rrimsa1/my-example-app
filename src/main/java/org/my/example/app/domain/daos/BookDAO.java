package org.my.example.app.domain.daos;

import org.my.example.app.domain.models.criteria.BookSearchCriteria;
import org.my.example.app.domain.models.entities.Author;
import org.my.example.app.domain.models.entities.Book;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class BookDAO extends AbstractDAO<Book> {
    public PageResult<Book> searchByCriteria(BookSearchCriteria criteria) {

        if (criteria == null) {
            throw new DAOException(
                    ErrorKeys.ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED,
                    new NullPointerException()
            );
        }
        try {

            CriteriaQuery<Book> cq = criteriaQuery();
            Root<Book> root = cq.from(Book.class);


            List<Predicate> predicates = new ArrayList<>();

            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();


            if (criteria.getId() != null && !criteria.getId().isEmpty()){
                predicates.add(cb.equal(root.get("id"), criteria.getId()));
            }
            if (criteria.getTitle() != null && !criteria.getTitle().isEmpty()) {
                predicates.add(cb.like(
                        cb.lower(root.get("title")),
                        criteria.getTitle().toLowerCase() + "%"));
            }


            if (criteria.getCategory() != null && !criteria.getCategory().isEmpty()) {
                predicates.add(cb.like(
                        cb.lower(root.get("category")),
                        criteria.getCategory().toLowerCase() + "%"));
            }

            if (criteria.getIsbn() != null) {
                predicates.add(cb.equal(root.get("isbn"), criteria.getIsbn()));
            }

            if (criteria.getNumberOfPages() != null) {
                predicates.add(cb.equal(root.get("numberOfPages"), criteria.getNumberOfPages()));
            }

            if (criteria.getAuthorId() != null) {
                predicates.add(
                        cb.equal(
                                root.get("author").get("id"),
                                criteria.getAuthorId()
                        )
                );
            }

            if (criteria.getDateOfPublication() != null) {
                predicates.add(
                        cb.lessThanOrEqualTo(
                                root.get("dateOfPublication"),
                                criteria.getDateOfPublication()
                        )
                );
            }


            if (!predicates.isEmpty()) {
                cq.where(predicates.toArray(new Predicate[0]));
            }

            return createPageQuery(cq, Page.of(criteria.getPageNumber(),
                    criteria.getPageSize())).getPageResult();

        } catch (Exception e) {
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOKS_BY_CRITERIA, e);
        }
    }

    enum ErrorKeys {
        ERROR_FIND_BOOKS_BY_CRITERIA,
        ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED
    }


    /**
     *
     * Find all books for a given author.
     *
     * @param author the {@link Author
     * @param expirationDate the booking end date.
     * @return list of books {@link Book} if find any for the given table in the given time frame.
     */
    public List<Book> getBookForGivenAuthor(Author author){
        CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        Root<Book> root = cq.from(Book.class);
        cq.where(
                cb.equal(root.get("author"), author)
        );
        TypedQuery<Book> typedQuery = em.createQuery(cq);
        return typedQuery.getResultList();
    }


}


