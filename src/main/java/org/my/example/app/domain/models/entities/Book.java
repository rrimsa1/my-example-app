package org.my.example.app.domain.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "T_BOOK")
public class Book extends TraceableEntity {

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "CATEGORY", nullable = false)
    private String category;

    @Column(name = "ISBN", nullable = false)
    private Integer isbn;

    @Column(name = "NUMBER_OF_PAGES")
    private Integer numberOfPages;

    @Column(name = "DATE_OF_PUBLICATION")
    private LocalDateTime dateOfPublication;

    @ManyToOne(fetch = FetchType.LAZY)
    private Author author;
}





