package org.my.example.app.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class AuthorSearchCriteria {

    private String id;

    private String surname;

    private String firstName;

    private Integer age;

    private LocalDateTime birthDate;

    /**
     * The number of page for Hibernate pagination.
     */
    private Integer pageNumber;
    /**
     * The size of page for Hibernate pagination.
     */
    private Integer pageSize;
}