package org.my.example.app.domain.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "T_AUTHOR")
public class Author extends TraceableEntity {

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;
    @Column(name = "SURNAME", nullable = false)
    private String surname;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "BIRTH_DATE")
    private LocalDateTime birthDate;
    @Column(name = "ADDRESS")
    private String address;


}