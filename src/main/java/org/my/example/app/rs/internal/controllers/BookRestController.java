package org.my.example.app.rs.internal.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.my.example.app.domain.daos.AuthorDAO;
import org.my.example.app.domain.daos.BookDAO;
import org.my.example.app.domain.models.entities.Author;
import org.my.example.app.rs.internal.mappers.BookMapper;
import org.my.example.app.rs.internal.models.BookCreateDTO;
import org.my.example.app.rs.internal.models.BookDTO;
import org.my.example.app.rs.internal.models.BookSearchCriteriaDTO;
import org.my.example.app.rs.internal.services.BookService;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;


@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Book Rest")
@Path("books")
public class BookRestController {

    private static final String AUTHOR_NOT_FOUND_ERROR = "Project with id= %s not found";

    @Inject
    BookService bookService;

    @Inject
    BookDAO bookDAO;

    @Inject
    BookMapper bookMapper;
    @Inject
    AuthorDAO authorDAO;

    @GET
    @Operation(operationId = "getBookByCriteria", description = "Get book by criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = PageResultDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response getBookByCriteria(@BeanParam BookSearchCriteriaDTO criteriaDTO) {
        PageResultDTO<BookDTO> pageResultDTO = bookService.getBooksByCriteria(criteriaDTO);
        return Response.ok(pageResultDTO).build();
    }

    @GET
    @Path("{id}")
    @Operation(operationId = "getBookById", description = "Get book by id")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDTO.class))),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response getBookById(@PathParam("id") String id) {
        BookDTO bookDTO = bookService.getBookById(id);
        return Response.status(Response.Status.OK).entity(bookDTO).build();
    }

    @POST
    @Operation(operationId = "createBook", description = "Create new book")
    @Transactional
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Created",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response createBook(@Valid BookCreateDTO bookCreateDTO) {

        BookDTO bookDTO = bookService.createBook(bookCreateDTO);
        if (!Objects.isNull(bookDTO.getAuthorId())) {
            Author bookAuthor = authorDAO.findById(bookDTO.getAuthorId());
            bookDTO.setAuthorId(bookAuthor.getId());
        }else {
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found.");
        }

        return Response.status(Response.Status.CREATED)
                .entity(bookDTO)
                .build();
        //throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found.");
    }

    @PUT
    @Path("{id}")
    @Operation(operationId = "updateBook", description = "Update existing book")
    @Transactional
    @APIResponses({
            @APIResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response updateBook(@PathParam("id") String id, @Valid BookDTO bookUpdateDTO) {
        BookDTO bookDTO = bookService.updateBookById(id, bookUpdateDTO);
        return Response.status(Response.Status.OK)
                .entity(bookDTO)
                .build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    @Operation(operationId = "deleteBook", description = "Delete the book")
    @APIResponses({
            @APIResponse(responseCode = "204", description = "No content"),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response deleteBook(@PathParam("id") String id) {
        bookService.deleteBookById(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}