package org.my.example.app.rs.internal.mappers;

import org.mapstruct.*;
import org.my.example.app.domain.models.criteria.BookSearchCriteria;
import org.my.example.app.domain.models.entities.Book;
import org.my.example.app.rs.internal.models.BookCreateDTO;
import org.my.example.app.rs.internal.models.BookDTO;
import org.my.example.app.rs.internal.models.BookSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;

import java.util.stream.Stream;

@Mapper(componentModel = "cdi", uses = OffsetDateTimeMapper.class,
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BookMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "authorId", ignore = true)
    BookDTO map(Book Entity);

    @Mapping(target = "id", ignore = true)
    Book map(BookDTO bookDTO);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "author", ignore = true)
    Book map(BookCreateDTO bookCreateDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "author", ignore = true)
    void update(BookDTO updatedBookDTO, @MappingTarget Book book);


    BookSearchCriteria map(BookSearchCriteriaDTO criteriaDTO);

    PageResultDTO<BookDTO> map(PageResult<Book> pageResult);
}