package org.my.example.app.rs.internal.services;

import org.my.example.app.domain.daos.AuthorDAO;
import org.my.example.app.domain.daos.BookDAO;
import org.my.example.app.domain.models.criteria.BookSearchCriteria;
import org.my.example.app.domain.models.entities.Author;
import org.my.example.app.domain.models.entities.Book;
import org.my.example.app.rs.internal.mappers.BookMapper;
import org.my.example.app.rs.internal.models.BookCreateDTO;
import org.my.example.app.rs.internal.models.BookDTO;
import org.my.example.app.rs.internal.models.BookSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.util.Objects;

@ApplicationScoped
public class BookService {

    private static final String AUTHOR_NOT_FOUND_ERROR = "Author with id= %s not found";
    private static final String BOOK_NOT_FOUND_ERROR = "Book with id= %s not found";

    private static final String BOOK_SEARCH_CRITERIA_NULL_ERROR = "Book search criteria cannot be null!";

    @Inject
    AuthorDAO authorDAO;
    @Inject
    BookDAO bookDAO;
    @Inject
    BookMapper bookMapper;




    public BookDTO getBookById(String id) {
        Book book = bookDAO.findById(id);
        if (Objects.isNull(book)) {
            throw new RestException(Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND,
                    String.format(BOOK_NOT_FOUND_ERROR, id));
        }
        return bookMapper.map(book);
    }

    @Transactional
    public BookDTO createBook(BookCreateDTO newBook) {

        Author author = authorDAO.findById(newBook.getAuthorId());

        //testet ob ein Autor mit der id überhaupt gefunden wird
        if(!Objects.isNull(author.getId())){

        }else{
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found in se sercvice.");
        }

        Book bookToCreate = bookMapper.map(newBook);

        bookToCreate.setAuthor(author);

        if (!Objects.isNull(newBook.getAuthorId())) {
            Author bookAuthor = authorDAO.findById(newBook.getAuthorId());
            if (Objects.isNull(bookAuthor)) {
                throw new RestException(Response.Status.NOT_FOUND,
                        Response.Status.NOT_FOUND,
                        String.format(AUTHOR_NOT_FOUND_ERROR, newBook.getAuthorId()));
            }


        }
        BookDTO bookDTO = bookMapper.map(bookDAO.create(bookToCreate));
        bookDTO.setAuthorId(author.getId());

        if(!Objects.isNull(bookDTO.getAuthorId())){

        }else{
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found in se sercvice.");
        }

        return bookDTO;
    }


    @Transactional
    public BookDTO updateBookById(String id, BookDTO updatedBookDTO) {
        Book bookToUpdate = bookDAO.findById(id);
        if (Objects.isNull(bookToUpdate)) {
            throw new RestException(Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND,
                    String.format(BOOK_NOT_FOUND_ERROR, id));
        }
        bookMapper.update(updatedBookDTO, bookToUpdate);
        return bookMapper.map(bookDAO.update(bookToUpdate));
    }


    @Transactional
    public void deleteBookById(String id) {
        Book bookToDelete = bookDAO.findById(id);
        if (Objects.isNull(bookToDelete)) {
            throw new RestException(Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND,
                    String.format(BOOK_NOT_FOUND_ERROR, id));
        }
        bookDAO.delete(bookToDelete);

    }


    public PageResultDTO<BookDTO> getBooksByCriteria(BookSearchCriteriaDTO criteriaDTO) {
        BookSearchCriteria bookSearchCriteria = bookMapper.map(criteriaDTO);
        PageResult<Book> books = bookDAO.searchByCriteria(bookSearchCriteria);
        if (Objects.isNull(books)) {
            throw new RestException(Response.Status.BAD_REQUEST,
                    Response.Status.BAD_REQUEST,
                    BOOK_SEARCH_CRITERIA_NULL_ERROR);
        }
        return bookMapper.map(books);
    }

}