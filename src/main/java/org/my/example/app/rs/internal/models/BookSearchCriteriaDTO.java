package org.my.example.app.rs.internal.models;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.my.example.app.domain.models.entities.Author;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import java.time.OffsetDateTime;

@Getter
@Setter
public class BookSearchCriteriaDTO {

    @QueryParam("id")
    @Schema(description = "The user id.")
    private String id;

    @Min(0)
    @QueryParam("page")
    @DefaultValue("0")
    private Integer pageNumber;

    @Min(1)
    @Max(100)
    @QueryParam("size")
    @DefaultValue("100")
    private Integer pageSize;

    @QueryParam("title")
    @Schema(description = "Title of the book")
    private String title;

    @QueryParam("category")
    @Schema(description = "Category of the book")
    private String category;

    @QueryParam("isbn")
    @Schema(description = "ISBN of the book")
    private Integer isbn;

    @QueryParam("numberOfPages")
    @Schema(description = "Number of pages in the book")
    private Integer numberOfPages;

    @QueryParam("dateOfPublication")
    @Schema(description = "Date of the publication")
    private OffsetDateTime dateOfPublication;

    @QueryParam("authorId")
    @Schema(description = "Book's Author's Id.")
    private String authorId;

//    wir suchen keine Bücher nach autoren
//    @QueryParam("author")
//    @Schema(description = "Author of the Book")
//    private String authorId;

}
