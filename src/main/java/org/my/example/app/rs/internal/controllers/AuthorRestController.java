package org.my.example.app.rs.internal.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.my.example.app.rs.internal.models.AuthorDTO;
import org.my.example.app.rs.internal.models.AuthorSearchCriteriaDTO;
import org.my.example.app.rs.internal.services.AuthorService;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@ApplicationScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Author Rest")
@Path("authors")
public class AuthorRestController {

    @Inject
    AuthorService authorService;

    @GET
    @Operation(operationId = "getAuthorByCriteria", description = "Get author by criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = PageResultDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response getAuthorByCriteria(@BeanParam AuthorSearchCriteriaDTO criteriaDTO) {
        PageResultDTO<AuthorDTO> pageResultDTO = authorService.getAuthorsByCriteria(criteriaDTO);
        return Response.ok(pageResultDTO).build();
    }

    @GET
    @Path("{id}")
    @Operation(operationId = "getAuthorById", description = "Get author by id")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response getAuthorById(@PathParam("id") String id) {
        AuthorDTO authorDTO = authorService.getAuthorById(id);
        return Response.status(Response.Status.OK).entity(authorDTO).build();
    }

    @POST
    @Operation(operationId = "createAuthor", description = "Create new author")
    @Transactional
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Created",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response createAuthor(@Valid AuthorDTO authorCreateDTO) {
        AuthorDTO authorDTO = authorService.createAuthor(authorCreateDTO);
        return Response.status(Response.Status.CREATED)
                .entity(authorDTO)
                .build();
    }

    @PUT
    @Path("{id}")
    @Operation(operationId = "updateAuthor", description = "Update existing author")
    @Transactional
    @APIResponses({
            @APIResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
            @APIResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response updateAuthor(@PathParam("id") String id, @Valid AuthorDTO authorUpdateDTO) {
        AuthorDTO authorDTO = authorService.updateAuthorById(id, authorUpdateDTO);
        return Response.status(Response.Status.OK)
                .entity(authorDTO)
                .build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    @Operation(operationId = "deleteAuthor", description = "Delete the author")
    @APIResponses({
            @APIResponse(responseCode = "204", description = "No content"),
            @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RestException.class)))
    })
    public Response deleteAuthor(@PathParam("id") String id) {
        authorService.deleteAuthorById(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}