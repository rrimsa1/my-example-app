package org.my.example.app.rs.internal.services;


import org.my.example.app.domain.daos.AuthorDAO;
import org.my.example.app.domain.models.criteria.AuthorSearchCriteria;
import org.my.example.app.domain.models.entities.Author;
import org.my.example.app.rs.internal.mappers.AuthorMapper;
import org.my.example.app.rs.internal.models.AuthorDTO;
import org.my.example.app.rs.internal.models.AuthorSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.util.Objects;

@ApplicationScoped
public class AuthorService {

    private static final String AUTHOR_NOT_FOUND_ERROR = "Author with id= %s not found";
    private static final String AUTHOR_SEARCH_CRITERIA_NULL_ERROR = "Author search criteria cannot be null!";
    @Inject
    AuthorDAO authorDAO;
    @Inject
    AuthorMapper authorMapper;


    public AuthorDTO getAuthorById(String id) {
        Author author = authorDAO.findById(id);
        if (Objects.isNull(author)) {
            throw new RestException(Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND,
                    String.format(AUTHOR_NOT_FOUND_ERROR, id));
        }
        return authorMapper.map(author);
    }


    @Transactional
    public AuthorDTO createAuthor(@Valid AuthorDTO newAuthorDTO) {
        Author authorToCreate = authorMapper.map(newAuthorDTO);

        return authorMapper.map(authorDAO.create(authorToCreate));
    }


    @Transactional
    public AuthorDTO updateAuthorById(String id, AuthorDTO updatedAuthorDTO) {
        Author authorToUpdate = authorDAO.findById(id);
        if (Objects.isNull(authorToUpdate)) {
            throw new RestException(Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND,
                    String.format(AUTHOR_NOT_FOUND_ERROR, id));
        }
        authorMapper.update(updatedAuthorDTO, authorToUpdate);
        return authorMapper.map(authorDAO.update(authorToUpdate));
    }


    @Transactional
    public void deleteAuthorById(String id) {
        Author authorToDelete = authorDAO.findById(id);
        if (Objects.isNull(authorToDelete)) {
            throw new RestException(Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND,
                    String.format(AUTHOR_NOT_FOUND_ERROR, id));
        }
        authorDAO.delete(authorToDelete);

    }


    public PageResultDTO<AuthorDTO> getAuthorsByCriteria(AuthorSearchCriteriaDTO criteriaDTO) {
        AuthorSearchCriteria authorSearchCriteria = authorMapper.map(criteriaDTO);
        PageResult<Author> authors = authorDAO.searchByCriteria(authorSearchCriteria);
        if (Objects.isNull(authors)) {
            throw new RestException(Response.Status.BAD_REQUEST,
                    Response.Status.BAD_REQUEST,
                    AUTHOR_SEARCH_CRITERIA_NULL_ERROR);
        }
        return authorMapper.map(authors);
    }

}