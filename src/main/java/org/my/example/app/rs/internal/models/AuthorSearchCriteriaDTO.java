package org.my.example.app.rs.internal.models;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import java.time.OffsetDateTime;

@Getter
@Setter
public class AuthorSearchCriteriaDTO {
    @Min(0)
    @QueryParam("page")
    @DefaultValue("0")
    private Integer pageNumber;

    @Min(1)
    @Max(100)
    @QueryParam("size")
    @DefaultValue("100")
    private Integer pageSize;

    @QueryParam("firstName")
    @Schema(description = "Authors first name")
    private String firstName;

    @QueryParam("surname")
    @Schema(description = "Authors first surname")
    private String surname;

    @QueryParam("age")
    @Schema(description = "Authors age")
    private Integer age;

    @QueryParam("birthDate")
    @Schema(description = "Authors birthdate")
    private OffsetDateTime birthDate;

    @QueryParam("address")
    @Schema(description = "Authors address")
    private String address;
}