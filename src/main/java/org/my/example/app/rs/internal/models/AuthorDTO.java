package org.my.example.app.rs.internal.models;


import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.rs.models.TraceableDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Getter
@Setter
public class AuthorDTO extends TraceableDTO {
    @NotBlank(message = "First name cannot be blank")
    private String firstName;

    @NotBlank(message = "Surname cannot be blank")
    private String surname;

    @Min(value = 0, message = "Age cannot by less then 0")
    private Integer age;

    @NotNull(message = "Must not be null")
    private OffsetDateTime birthDate;

    private String address;
}