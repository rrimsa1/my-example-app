package org.my.example.app.rs.internal.mappers;


import org.mapstruct.*;
import org.my.example.app.domain.models.criteria.AuthorSearchCriteria;
import org.my.example.app.domain.models.entities.Author;
import org.my.example.app.rs.internal.models.AuthorDTO;
import org.my.example.app.rs.internal.models.AuthorSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;

@Mapper(componentModel = "cdi", uses = OffsetDateTimeMapper.class,
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AuthorMapper {

    AuthorDTO map(Author author);

    @Mapping(target = "id", ignore = true)
    Author map(AuthorDTO authorDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "id", ignore = true)
    void update(AuthorDTO updatedAuthorDTO, @MappingTarget Author author);


    AuthorSearchCriteria map(AuthorSearchCriteriaDTO criteriaDTO);

    PageResultDTO<AuthorDTO> map(PageResult<Author> pageResult);
}