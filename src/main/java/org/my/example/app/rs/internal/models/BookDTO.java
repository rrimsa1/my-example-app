package org.my.example.app.rs.internal.models;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.my.example.app.domain.models.entities.Author;
import org.tkit.quarkus.rs.models.TraceableDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Getter
@Setter
public class BookDTO extends TraceableDTO {

    @NotBlank(message = "title cannot be blank")
    private String title;

    @NotBlank(message = "Category cannot be blank")
    private String category;

    @Min(value = 0, message = "ISBN cannot by less then 0")
    private Integer isbn;

    @Min(value = 0, message = "Number of Pages cannot by less then 0")
    private Integer numberOfPages;

    @NotNull(message = "Date of Publication must not be null")
    private OffsetDateTime dateOfPublication;

    @NotNull(message = "Id of the Author assigned to the book")
    private String authorId;
}